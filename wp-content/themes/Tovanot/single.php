<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$currentType = get_post_type($postId);
?>
<article class="page-body article-page-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container pt-4">
		<div class="row justify-content-between align-items-stretch">
			<div class="col-xl col-lg-7 col-12 d-flex flex-column align-items-start">
				<div class="base-output page-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-xl-auto col-lg-5 col-12 mt-md-0 mt-4">
				<div class="sticky-form">
					<div class="form-wrapper-pop form-item-wrap">
						<span class="close-form close-pop">
						</span>
						<?php if ($f_title = opt('post_form_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('post_form_subtitle')) : ?>
							<h3 class="pop-form-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						getForm('18'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($fields['video_link']) : ?>
			<div class="row">
				<div class="col-12">
					<div class="video-trigger-wrap mb-4">
						<span class="play-button" data-video="<?= getYoutubeId($fields['video_link']); ?>"
						<?php if ($thumb = getYoutubeThumb($fields['video_link'])) : ?>
							style="background-image: url('<?= $thumb; ?>')"
						<?php endif; ?>>
							<img src="<?=ICONS ?>play.png" alt="play-button">
						</span>
					</div>
					<h3 class="video-description">
						<?= $fields['video_description'] ? $fields['video_description'] : ''; ?>
					</h3>
				</div>
			</div>
		<?php endif; ?>
			<div class="row">
				<div class="col">
					<div class="trigger-wrap">
						<a class="social-trigger">
						<span class="social-item">
							<img src="<?= ICONS ?>trigger-social.png" alt="share">
						</span>
							<span class="social-item-text">
							<?= esc_html__('שתפו את הפוסט הזה עם חברים', 'leos'); ?>
						</span>
						</a>
						<div class="all-socials item-socials" id="show-socials">
							<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
							   class="socials-wrap social-item">
								<i class="fa-brands fa-whatsapp"></i>
							</a>
							<a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
							   target="_blank">
								<i class="fa-brands fa-facebook"></i>
							</a>
							<a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
								<i class="fa-solid fa-circle-envelope"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
	</div>
</article>
<?php $taxname = 'category';
switch ($currentType) {
	case 'treatment':
		$taxname = 'treatment_cat';
		$text = 'טיפולים נוספים';
		$sameTitle = '<h2>'.$text.'</h2>';
		break;
	case 'post':
		$taxname = 'category';
		$text = 'מאמרים נוספים';
		$sameTitle = '<h2>'.$text.'</h2>';
		break;
	default:
		$taxname = 'category';
		$text = 'מאמרים נוספים';
		$sameTitle = '<h2>'.$text.'</h2>';
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 5,
	'post_type' => $currentType,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $taxname,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 5,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'articles_slider', [
		'text' => $fields['same_text'] ? $fields['same_text'] : $sameTitle,
		'posts' => $samePosts,
	]);
} ?>
<div class="dark-treatments-back">
	<?php get_template_part('views/partials/repeat', 'treatments', [
		'title' => $fields['treat_cats_title'],
		'text' => $fields['treat_cats_text'],
		'items' => $fields['treat_cats'],
		'link' => $fields['treat_cats_link'],
	]); ?>
</div>
<?php get_footer(); ?>
