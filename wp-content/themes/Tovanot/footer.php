<?php
$facebook = opt('facebook');
$whatsapp = opt('whatsapp');
$instagram = opt('instagram');
$links_menu_title = opt('foo_menu_title');
?>
<footer>
	<div class="foo-back">
		<div class="foo-form-block">
			<img src="<?= IMG ?>top-underline.png" alt="wave" class="foo-top-line">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php if($f_title = opt('foo_form_title')) : ?>
							<h2 class="form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('foo_form_subtitle')) : ?>
							<h3 class="form-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						getForm('20'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-main">
			<a id="go-top">
				<img src="<?= ICONS ?>to-top.png">
			</a>
			<div class="container footer-container-menu">
				<div class="row justify-content-sm-between justify-content-center align-items-stretch foo-after-title">
					<div class="col-lg-2 col-6 foo-menu">
						<h3 class="foo-title">
							<?= esc_html__('ניווט מהיר', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-menu', '1'); ?>
						</div>
					</div>
					<div class="col-lg-auto col-6 foo-menu services-footer-menu">
						<h3 class="foo-title">
							<?= esc_html__('שירותינו', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-services-menu', '1'); ?>
						</div>
					</div>
					<div class="col-lg-auto col-12 foo-menu foo-links-menu">
						<h3 class="foo-title">
							<?= $links_menu_title ? $links_menu_title : esc_html__('חם מהבלוג', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
