<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();

?>

<article class="home-about-block">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="<?= $fields['about_img_block'] ? 'col-lg-7 col-12' : 'col-12'; ?> d-flex flex-column justify-content-start align-items-center">
				<div class="base-output mb-3">
					<h1 class="base-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<?php if ($fields['about_img_block']) : ?>
				<div class="col-xl-4 col-lg-5 mt-lg-0 mt-4">
					<img src="<?= $fields['about_img_block']['url']; ?>" alt="about-image" class="home-about-img">
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form', [
	'about' => true,
]);
if ($fields['base_reviews_text'] || $fields['base_review_item']) {
	get_template_part('views/partials/repeat', 'benefits', [
		'reviews' => $fields['base_review_item'],
		'text' => $fields['base_reviews_text']
	]);
}
if ($fields['about_services_items'] || $fields['about_services_text']) {
	get_template_part('views/partials/repeat', 'benefits', [
		'items' => $fields['about_services_items'],
		'text' => $fields['about_services_text'],
		'link' => $fields['about_services_link']
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'title' => $fields['single_slider_title'],
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
