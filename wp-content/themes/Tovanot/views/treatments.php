<?php
/*
Template Name: טיפולים
*/

get_header();
$fields = get_fields();
$terms = get_terms([
	'taxonomy'      => 'treatment_cat',
	'hide_empty'    => false,
	'parent'        => 0
]);
?>
<article class="page-body treatments-page">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($terms) : ?>
			<div class="row align-items-stretch justify-content-center">
				<?php foreach ($terms as $item) : $img = get_field('cat_img', $item);
				$posts = get_posts([
						'post_type' => 'treatment',
						'numberposts' => '5',
						'suppress_filters' => false,
						'tax_query' => [
								[
										'taxonomy' => 'treatment_cat',
										'field' => 'term_id',
										'terms' => $item->term_id,
								]
						]
				]); ?>
					<div class="col-lg-6 col-12 col-category">
						<div class="card__category-page" <?php if ($img) : ?>
							style="background-image: url('<?= $img['url']; ?>')"
						<?php endif; ?>>
							<span class="card__cat-overlay"></span>
							<div class="card__cat-page-content">
								<?php if ($posts) : ?>
									<div class="posts__cat-items">
										<?php foreach ($posts as $post) : ?>
											<a href="<?= get_the_permalink($post); ?>" class="post__inside-link">
												<?= $post->post_title; ?>
											</a>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
								<a class="card__category-name card__cat-page-name" href="<?= get_term_link($item); ?>">
									<?= $item->name; ?>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="red-back-block">
	<?php get_template_part('views/partials/repeat', 'animals', [
			'text' => $fields['animals_text'],
			'link' => $fields['animals_link'],
			'img' => $fields['animals_img'],
	]); ?>
</div>
<?php get_footer(); ?>

