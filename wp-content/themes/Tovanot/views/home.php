<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
?>
<section class="home-main-block">
	<?php if ($fields['h_main_slider']) : ?>
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['h_main_slider'] as $slide) : ?>
				<?php $img = ''; $video = '';
				if (isset($slide['main_back']['0'])) {
					if ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_img') {
						$img = isset($slide['main_back']['0']['img']) ? $slide['main_back']['0']['img'] : '';
					} elseif ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_video') {
						$video = isset($slide['main_back']['0']['video']) ? $slide['main_back']['0']['video'] : '';
					}
				}
				if ($video) : ?>
					<div class="slide-video">
						<video muted autoplay="autoplay" loop="loop">
							<source src="<?= $video['url']; ?>" type="video/mp4">
						</video>
						<div class="container">
							<div class="row justify-content-start">
								<div class="col-xl-9 col-lg-10 col-12">
									<?php if ($slide['main_home_text']) : ?>
										<div class="base-output homepage-main-text">
											<?= $slide['main_home_text']; ?>
										</div>
									<?php endif;
									if ($slide['main_home_link']) : ?>
										<div class="row justify-content-start">
											<div class="col-auto">
												<a href="<?= isset($slide['main_home_link']['url']) ? $slide['main_home_link']['url'] : ''; ?>" class="base-link regular-link">
													<?= isset($slide['main_home_link']['title']) ? $slide['main_home_link']['title'] : esc_html__('ליצירת קשר', 'leos'); ?>
												</a>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="slide-main" style="background-image: url('<?= $img ? $img['url'] : ''; ?>')">
						<div class="container">
							<div class="row justify-content-start">
								<div class="col-xl-9 col-lg-10 col-12">
									<?php if ($slide['main_home_text']) : ?>
										<div class="base-output homepage-main-text">
											<?= $slide['main_home_text']; ?>
										</div>
									<?php endif;
									if ($slide['main_home_link']) : ?>
										<div class="row justify-content-start">
											<div class="col-auto">
												<a href="<?= isset($slide['main_home_link']['url']) ? $slide['main_home_link']['url'] : ''; ?>" class="base-link regular-link">
													<?= isset($slide['main_home_link']['title']) ? $slide['main_home_link']['title'] : esc_html__('ליצירת קשר', 'leos'); ?>
												</a>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif;
			endforeach; ?>
		</div>
	<?php endif; ?>
	<img src="<?= IMG ?>top-underline.png" alt="underline-wave" class="bottom-main">
</section>
<?php if ($fields['h_about_text']) : ?>
	<section class="about-block">
		<div class="container">
			<div class="row justify-content-center align-items-center row-mob-reverse">
				<div class="col-lg-10 col-12">
					<div class="base-output text-center">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_benefits']) : ?>
						<div class="row justify-content-center align-items-start">
							<?php foreach ($fields['h_about_benefits'] as $benefit) : ?>
								<div class="col-md-4 col-12 benefit-col">
									<div class="benefit-item">
										<div class="benefit-img-wrapper">
											<?php if ($benefit['img']) : ?>
												<img src="<?= $benefit['img']['url']; ?>" alt="<?= $benefit['img']['alt']; ?>">
											<?php endif; ?>
										</div>
										<h3 class="benefit-title">
											<?= $benefit['desc']; ?>
										</h3>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif;
					if ($fields['h_about_link']) : ?>
						<div class="row justify-content-center">
							<div class="col-auto">
								<a href="<?= $fields['h_about_link']['url'];?>" class="regular-link">
									<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
											? $fields['h_about_link']['title'] : esc_html__('עוד אודתינו', 'leos');
									?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_team_items'] || $fields['h_team_title'] || $fields['h_team_link']) : ?>
	<section class="team__slider-block arrows-slider">
		<div class="container">
			<?php if ($fields['h_team_title']) : ?>
				<div class="row">
					<div class="col-12">
						<h2 class="figure-title">
							<?= $fields['h_team_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-sm-10 col-12">
					<?php if ($fields['h_team_text']) : ?>
						<div class="base-output white-output text-center">
							<?= $fields['h_team_text']; ?>
						</div>
					<?php else: ?>
						<h3 class="block-title">
							<?php esc_html__('צוות המומחים שלנו', 'leos'); ?>
						</h3>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($fields['h_team_items']) : ?>
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-10">
						<div class="team-slider" dir="rtl">
							<?php foreach ($fields['h_team_items'] as $team) : ?>
								<div>
									<?php get_template_part('views/partials/card', 'member', [
											'post' => $team,
									]); ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif;
		if ($fields['h_team_link']) : ?>
			<div class="container">
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $fields['h_team_link']['url'];?>" class="regular-link">
							<?= (isset($fields['h_team_link']['title']) && $fields['h_team_link']['title'])
									? $fields['h_team_link']['title'] : esc_html__('לכל המטפלים', 'leos');
							?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>
<section class="custom-bookly-block">
	<div class="container">
		<?php if ($reg_title = opt('reg_title')) : ?>
			<div class="row justify-content-center">
				<div class="col">
					<h2 class="registration-title">
						<?= $reg_title; ?>
					</h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-12">
				<?php echo do_shortcode('[bookly-form]'); ?>
			</div>
		</div>
	</div>
</section>
<div class="treatments-home-block">
	<?php get_template_part('views/partials/repeat', 'treatments', [
			'title' => $fields['treat_cats_title'],
			'text' => $fields['treat_cats_text'],
			'items' => $fields['treat_cats'],
			'link' => $fields['treat_cats_link'],
	]);
	?>
</div>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'title' => $fields['single_slider_title'],
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_template_part('views/partials/repeat', 'form');
if ($fields['h_posts']) {
	get_template_part('views/partials/content', 'articles_slider', [
			'text' => $fields['h_posts_text'],
			'link' => $fields['h_posts_link'],
			'posts' => $fields['h_posts'],
			'link_title' => esc_html__('לכל המאמרים', 'leos'),
	]);
}
get_template_part('views/partials/repeat', 'animals');
get_template_part('views/partials/repeat', 'reviews', [
		'text' => $fields['base_reviews_text'],
		'items' => $fields['base_review_item']
]);
if ($fields['h_join_text'] || $fields['h_join_img']) :
$join_link = $fields['h_join_link']; ?>
	<section class="join-block">

	</section>
<?php endif;
get_footer(); ?>
