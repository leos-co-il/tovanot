<?php
/*
Template Name: קלפים
*/

get_header();
$fields = get_fields();
$cards = $fields['page_cards'];
$quantity = $cards ? count($cards) : '';
?>
<article class="page-body animal-cards-page">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-md-10 col-sm-11 col-12">
				<div class="base-output base-output-cards text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($cards) : ?>
			<div class="row align-items-stretch justify-content-center put-here-posts">
				<?php foreach ($cards as $x => $card) {
					if ($x < 9) {
						get_template_part('views/partials/card', 'animal',
								[
										'item' => $card,
						]);
					}
				} ?>
			</div>
		<?php endif;
		if ($quantity > 9) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link base-link load-more-posts more-link-cards" data-type="animal" data-count="<?= $quantity; ?>" data-page="<?= get_queried_object_id(); ?>">
						<?= esc_html__('טען עוד..', 'leos'); ?>
						<img src="<?= ICONS ?>down.png" alt="download-more">
					</div>
				</div>
			</div>
		<?php endif;
		if ($fields['questions_text'] || $fields['questions_items']) : ?>
			<div class="row steps-block">
				<?php if ($fields['questions_text']) : ?>
					<div class="col-12">
						<div class="base-output steps-text-output">
							<?= $fields['questions_text']; ?>
						</div>
					</div>
				<?php endif;
				if ($fields['questions_items']) : ?>
					<div class="col-12 arrows-slider">
						<div class="steps-slider" dir="rtl">
							<?php foreach ($fields['questions_items'] as $y => $question) : ?>
								<div class="steps-slide">
									<div class="question-item">
										<div class="question-number">
											<?= '0'.($y + 1); ?>
										</div>
										<h3 class="question-title">
											<?= $question['question']; ?>
										</h3>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif;
		if ($fields['question_after_text']) : ?>
			<div class="row">
				<div class="col-12">
					<h3 class="thank-you-text">
						<?= $fields['question_after_text']; ?>
					</h3>
				</div>
			</div>
		<?php endif;
		if ($logo = opt('logo')) : ?>
			<div class="row justify-content-center mb-4">
				<div class="col-xl-3 col-lg-4 col-sm-5 col-7">
					<a href="<?= home_url(); ?>" class="logo">
						<img src="<?= $logo['url']; ?>" alt="logo">
					</a>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-7 col-sm-9">
				<?php getForm('19'); ?>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['more_articles']) : ?>
	<div class="dark-slider-back">
		<?php get_template_part('views/partials/content', 'articles_slider', [
			'text' => $fields['more_articles_text'] ? $fields['more_articles_text'] : esc_html__('מאמרים נוספים', 'leos'),
			'posts' => $fields['more_articles'] ? $fields['more_articles'] : get_posts(['post_type' => 'post', 'numberposts' => '5']),
		]); ?>
	</div>
<?php endif;
get_footer(); ?>

