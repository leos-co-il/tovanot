<?php if (isset($args['posts']) && $args['posts']) :
	$text = (isset($args['text']) && $args['text']) ? $args['text'] : '';
	$link = (isset($args['link']) && $args['link']) ? $args['link'] : '';
	$link_title = (isset($args['link_title']) && $args['link_title']) ? $args['link_title'] : '';
	?>
	<div class="home-posts-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-md-8 col-sm-10 col-12">
					<?php if ($text) : ?>
						<div class="base-output text-center">
							<?= $text; ?>
						</div>
					<?php else: ?>
						<h2 class="block-title">
							<?php esc_html__('מאמרים נוספים', 'leos'); ?>
						</h2>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch arrows-slider">
				<div class="col-12">
					<div class="posts-slider" dir="rtl">
						<?php foreach ($args['posts'] as $post) : ?>
							<div>
								<?php get_template_part('views/partials/card', 'post',
									[
										'post' => $post,
									]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-center mt-5">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="regular-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : $link_title; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
