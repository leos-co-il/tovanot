<section class="top-block" <?php if (isset($args['img']) && $args['img']) : ?>
	style="background-image: url('<?= $args['img']; ?>')"
<?php endif; ?>>
	<span class="top-overlay"></span>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="page-title"><?= (isset($args['title']) && $args['title']) ? $args['title'] : ''; ?></h1>
			</div>
		</div>
	</div>
	<img src="<?= IMG ?>top-underline.png" alt="underline-wave" class="bottom-main">
</section>
