<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-output-col">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-card-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-card-content">
				<div class="card-content-wrapper">
					<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
					<p class="card-text">
						<?= text_preview($args['post']->post_content, 10); ?>
					</p>
				</div>
				<a href="<?= $link; ?>" class="base-link post-card-link">
					<?= esc_html__('קרא עוד ', 'leos'); ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
