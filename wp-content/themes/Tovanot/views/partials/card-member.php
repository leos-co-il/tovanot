<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="post-card card-member">
		<a class="post-card-image" href="<?= $link; ?>"
			<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif;?>>
		</a>
		<div class="post-card-content">
			<div class="card-content-wrapper">
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="card__member-text">
					<?= get_field('specialization', $args['post']->ID); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="base-link">
				<?= esc_html__('למטפל', 'leos'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
