<?php
$text = (isset($args['text']) && $args['text']) ? $args['text'] : opt('treat_cats_text');
$title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('treat_cats_title');
$link = (isset($args['link']) && $args['link']) ? $args['link'] : opt('treat_cats_link');
$items = (isset($args['items']) && $args['items']) ? $args['items'] : opt('treat_cats');
$link_title = esc_html__('לכל הטיפולים', 'leos');
if ($text || $items) : ?>
<div class="home-posts-block">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if ($title) : ?>
					<h2 class="figure-title">
						<?= $title; ?>
					</h2>
				<?php endif;
				if ($text) : ?>
					<div class="base-output text-center">
						<?= $text; ?>
					</div>
				<?php else: ?>
					<h3 class="block-title">
						<?php esc_html__('תחומי הטיפול שלנו', 'leos'); ?>
					</h3>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($link) : ?>
			<div class="row justify-content-center mb-4">
				<div class="col-auto">
					<a href="<?= $link['url'];?>" class="regular-link">
						<?= (isset($link['title']) && $link['title']) ? $link['title'] : $link_title; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($items) : ?>
		<div class="treatments-output">
			<?php foreach ($items as $item) : $img = get_field('cat_img', $item); ?>
				<div class="card-category">
					<div class="card__category-item" <?php if ($img) : ?>
						style="background-image: url('<?= $img['url']; ?>')"
					<?php endif; ?>>
						<div class="card__category-content">
							<div class="flex-grow-1">
								<h4 class="card__category-name">
									<?= $item->name; ?>
								</h4>
								<p class="card__category-text">
									<?= text_preview(category_description($item->term_id), '15'); ?>
								</p>
							</div>
							<a href="<?= get_term_link($item); ?>" class="base-link">
								<?= esc_html__('קרא עוד ', 'leos'); ?>
							</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>
<?php endif; ?>
