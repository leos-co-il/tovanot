<?php if(isset($args['item']) && $args['item']) :  ?>
	<div class="col-lg-4 col-sm-6 col-12 col-animal">
		<div class="card-animal more-card" data-id="<?= isset($args['num']) && $args['num'] ? $args['num'] : '' ?>">
			<img src="<?= $args['item']['url']; ?>" alt="<?= $args['item']['title']; ?>" class="image-animal">
		</div>
	</div>
<?php endif; ?>
