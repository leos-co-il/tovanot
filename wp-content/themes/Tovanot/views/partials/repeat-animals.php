<?php $text = (isset($args['text']) && $args['text']) ? $args['text'] : opt('animals_text');
$link = (isset($args['link']) && $args['link']) ? $args['link'] : opt('animals_link');
$img = (isset($args['img']) && $args['img']) ? $args['img'] : opt('animals_img');
$link_title = esc_html__('למעבר לקלפי החיות', 'leos');
if ($text) : ?>
	<section class="animal-block">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="<?= $img ? 'col-xl-4 col-lg-6 col-12' : 'col-12'; ?>">
					<div class="base-output white-output">
						<?= $text; ?>
					</div>
					<?php if ($link) : ?>
						<div class="row justify-content-start">
							<div class="col-auto">
								<a href="<?= $link['url'];?>" class="base-link">
									<?= (isset($link['title']) && $link['title']) ? $link['title'] : $link_title; ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($img) : ?>
					<div class="col-xl-8 col-lg-6 col-12 image-animals-col">
						<img src="<?= $img['url']; ?>" alt="animal-cards">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
