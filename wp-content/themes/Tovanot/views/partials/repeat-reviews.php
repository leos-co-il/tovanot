<?php
$text = (isset($args['text']) && $args['text']) ? $args['text'] : opt('base_reviews_text');
$items = (isset($args['items']) && $args['items']) ? $args['items'] : opt('base_review_item');
if ($text || $items) : ?>
<section class="reviews-block arrows-slider">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if ($text) : ?>
					<div class="base-output text-center">
						<?= $text; ?>
					</div>
				<?php else: ?>
					<h3 class="block-title">
						<?php esc_html__('מטופלים ממליצים', 'leos'); ?>
					</h3>
				<?php endif; ?>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-10 col-12">
				<div class="reviews-block-wrap">
					<div class="base-slider" dir="rtl">
						<?php foreach ($items as $review) : ?>
							<div class="slide-rev">
								<div class="review-item">
									<div class="rev-content">
										<h3 class="review-name"><?= $review['name']; ?></h3>
										<h4 class="review-description">
											<?= $review['service_name']; ?>
										</h4>
										<p class="review-prev">
											<?= text_preview($review['service_text'], '40'); ?>
										</p>
										<div class="rev-pop-trigger">
											+
											<div class="hidden-review">
												<div class="base-output">
													<?= $review['service_text']; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Reviews pop-up-->
<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i class="fas fa-times"></i>
			</button>
			<div class="modal-body" id="reviews-pop-wrapper"></div>
		</div>
	</div>
</div>
<?php endif; ?>
