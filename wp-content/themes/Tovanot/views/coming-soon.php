<?php/* Template Name: Coming soon page */?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Leos Group</title>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Leos coming soon" />
	<meta name="author" content="Leos" />
	<link rel="shortcut icon" href="../favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?= CS_PAGE_ASSETS ?>demo.css" />
	<link rel="stylesheet" type="text/css" href="<?= CS_PAGE_ASSETS ?>style1.css" />
	<link href='http://fonts.googleapis.com/css?family=Unlock|Cookie' rel='stylesheet' type='text/css' />
	<!--[if lt IE 10]>
	<link rel="stylesheet" type="text/css" href="<?= CS_PAGE_ASSETS ?>style1IE.css" />
	<![endif]-->
</head>
<body>
<div class="container">
	<div class="sp-container">
		<div class="sp-content">
			<div class="sp-row">
				<span>
					<img src="<?= CS_PAGE_ASSETS ?>logo-1.png" />
					<img src="<?= CS_PAGE_ASSETS ?>logo-2.png" />
				</span>
				<span>
					<img src="<?= CS_PAGE_ASSETS ?>logo-3.png" />
					<img src="<?= CS_PAGE_ASSETS ?>logo-4.png" />
				</span>
				<span>
					<img src="<?= CS_PAGE_ASSETS ?>logo-5.png" />
					<img src="<?= CS_PAGE_ASSETS ?>logo-6.png" />
				</span>
			</div>
			<div class="sp-row sp-side-row">

			</div>
			<div class="sp-row sp-content-row">
				<h1>Coming Soon</h1>
				<h2></h2>
				<h1 class="sp-title"><img src="<?= CS_PAGE_ASSETS ?>leos_main.png" alt=""></h1>
			</div>
			<div class="sp-row sp-side-row">

			</div>
			<div class="sp-row">
				<span>
					<img src="<?= CS_PAGE_ASSETS ?>logo-7.png" />
					<img src="<?= CS_PAGE_ASSETS ?>logo-8.png" />
				</span>
				<span>
					<img src="<?= CS_PAGE_ASSETS ?>logo-9.png" />
					<img src="<?= CS_PAGE_ASSETS ?>logo-10.png" />
				</span>
				<span>
					<img src="<?= CS_PAGE_ASSETS ?>logo-11.png" />
					<img src="<?= CS_PAGE_ASSETS ?>logo-12.png" />
				</span>
			</div>
			<div class="sp-arrow"></div>
		</div>
		<div class="sp-side">
			<h2>Be the first to know:</h2>
			<div class="sp-input">
				<input type="text" value="Your email"/>
				<a href="index.html">Go</a>
			</div>
		</div>
	</div>
</div>
</body>
</html>
