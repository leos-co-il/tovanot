<?php
/*
Template Name: פוסטים
*/

get_header();
$fields = get_fields();
$post_type = $fields['type'] ? $fields['type'] : 'post';
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => $post_type,
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => $post_type,
	'suppress_filters' => false,
]);
?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container mt-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post_col',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link regular-link load-more-posts" data-type="<?= $post_type; ?>" data-count="<?= $num; ?>">
						<?= esc_html__('טען עוד..', 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="dark-treatments-back">
	<?php get_template_part('views/partials/repeat', 'treatments', [
			'title' => $fields['treat_cats_title'],
			'text' => $fields['treat_cats_text'],
			'items' => $fields['treat_cats'],
			'link' => $fields['treat_cats_link'],
	]); ?>
</div>
<?php get_footer(); ?>

