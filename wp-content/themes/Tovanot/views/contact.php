<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$facebook = opt('facebook');
$instagram = opt('instagram');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-12">
				<div class="row justify-content-center">
					<div class="col-xl-6 d-flex flex-column justify-content-between contacts-column">
						<?php if ($fields['contact_items_title']) : ?>
							<h3 class="form-title">
								<?= $fields['contact_items_title']; ?>
							</h3>
						<?php endif; ?>
						<?php if ($tel) : ?>
							<div class="contact-item contact-item-link wow fadeInUp"
								 data-wow-delay="0.2s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<div class="contact-info-wrap">
									<a href="tel:<?= $tel; ?>" class="contact-info">
										<?= $tel; ?>
									</a>
								</div>
							</div>
						<?php endif;
						if ($mail) : ?>
							<div class="contact-item contact-item-link wow fadeInUp"
								 data-wow-delay="0.6s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<div class="contact-info-wrap">
									<a href="mailto:<?= $mail; ?>" class="contact-info">
										<?= $mail; ?>
									</a>
								</div>
							</div>
						<?php endif;
						if ($facebook) : ?>
							<div class="contact-item wow fadeInUp" data-wow-delay="0.4s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>facebook.png">
								</div>
								<div class="contact-info-wrap">
									<p class="contact-info">
										<?= $facebook; ?>
									</p>
								</div>
							</div>
						<?php endif;
						if ($instagram) : ?>
							<div class="contact-item wow fadeInUp" data-wow-delay="1s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>instagram.png">
								</div>
								<div class="contact-info-wrap">
									<p class="contact-info">
										<?= $instagram; ?>
									</p>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-xl-6">
						<div class="contact-form-wrap">
							<?php if ($fields['contact_form_title']) : ?>
								<h2 class="form-title">
									<?= $fields['contact_form_title']; ?>
								</h2>
							<?php endif;
							if ($fields['contact_form_subtitle']) : ?>
								<h2 class="form-subtitle">
									<?= $fields['contact_form_subtitle']; ?>
								</h2>
							<?php endif;
							getForm('103'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
